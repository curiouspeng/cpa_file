$(function () {
    user_list_reload();
    tx_order_list();
});
function user_list_reload(){
_table = $('#user_table_1').dataTable({
            processing: true,
            serverSide: true,
            bFilter: false,
            bSort: false,
            bInfo: true,
            bStateSave: false,
            scrollY: 400,
            "bAutoWidth": false,
            ajax: {
                url: '/main/memberview/userlist',
                type: 'POST',
                data: function (d) {
                    if ($('.sTime').val() != '') {
                        d.search['stime'] = $('.sTime').val();
                    }
                    if ($('.eTime').val() != '') {
                        d.search['etime'] = $('.sTime').val();
                    }
                    if($('#user_id').val() !=''){
                        d.search['user_id'] = $('#user_id').val();
                    }
                    if($('#master_id').val() !=''){
                        d.search['master_id'] = $('#master_id').val();
                    }
                    if($("select[name='user_status']").val() != 0){
                        d.search['stauts']=$("select[name='user_status']").val();
                    }
                }
            },
            columns: [
                {
                    "data": "userId",
                    render:function (data,type,full) {
                       return "<input type='checkbox' class='checkboxes' value='" + full.userId + "' />";
                    }

                },
                {
                    "data": "userId"
                },
                {
                    "data":"nickName"
                },
                {
                    "data": "score"
                },
                {
                    "data": "totalScore"
                },
                {
                    "data": "master"
                },
                {
                    "data": "status",
                    render: function (data, type, full) {
                        var status_text = "<span class='label label-success user-status' data-status="+full.status+">活跃</span>";
                        switch (full.status) {
                            case 2:
                                status_text = "<span class='label label-default user-status' data-status="+full.status+">死粉</span>";
                                break;
                            case 3:
                                status_text = "<span class='label label-danger user-status' data-status="+full.status+">冻结</span>";
                                break;
                        }
                       return status_text;
                    }
                },
                {
                    "data": "regTime"
                },
                {
                    "data": "lastTime"
                }
                /*,{

                    "data":"userId",
                    "render":function(data,type,full){
                        var html = "<a  href=\"javascript:;\" class=\"ajax-model-demo\" data-url=\"/main/memberview/userbehaviorrecord?user="+full.userId+"\" data-toggle=\"modal\"> 行为记录 </a>";
                        html +="<a  href=\"javascript:;\"> 金额记录 </a>";
                        html +="<a href='javascript:;' class=\"ajax-model-demo\" data-url=\"/main/memberview/userfans?user="+full.userId+"\" data-toggle=\"modal\">粉丝关系</a>"
                        if(full.status ==3){
                            html +="<a data-id='"+full.userId+"' class=\"ajax-model-demo\" data-url=\"/main/memberview/userblocked?user="+full.userId+"&status=1\" data-toggle=\"modal\"> 解冻 </a>"
                        }else{
                            html +="<a data-id='"+full.userId+"' class=\"ajax-model-demo\" data-url=\"/main/memberview/userblocked?user="+full.userId+"&status=-1\" data-toggle=\"modal\"> 冻结 </a>"
                        }

                       return html;
                    }
                }*/
            ],
            "fnDrawCallback": function (oSettings) {
               UpdateUIExtendedModals.init();
                $(document).bind("contextmenu", function (e) {
                return false;
                });
                right_menu();
            }
        });
    }
function tx_order_list(){
_table = $('#order_table_1').dataTable({
            processing: true,
            serverSide: true,
            bFilter: false,
            bSort: false,
            bInfo: true,
            bStateSave: false,
            scrollY: 400,
            "bAutoWidth": false,
            ajax: {
                url: '/main/memberview/orderlist',
                type: 'POST',
                data: function (d) {
                    if ($('.sTime').val() != '') {
                        d.search['stime'] = $('.sTime').val();
                    }
                    if ($('.eTime').val() != '') {
                        d.search['etime'] = $('.sTime').val();
                    }
                    if($('#user_id').val() !=''){
                        d.search['user_id'] = $('#user_id').val();
                    }
                    if($('#order_id').val() !=''){
                        d.search['order_id'] = $('#order_id').val();
                    }
                    if($("select[name='order_status']").val() != 0){
                        d.search['stauts']=$("select[name='order_status']").val();
                    }
                }
            },
            columns: [
                {
                    "data": "orderId",
                    render:function (data,type,full) {
                       return "<input type='checkbox' class='checkboxes' value='" + full.userId + "'' />";
                    }

                },
                {
                    "data": "orderId"
                },
                {
                    "data": "userId"
                },
                {
                    "data":"orderMoney"
                },
                {
                    "data": "orderType",
                    render:function(data,type,full){
                       //var txt="支付宝";
                        return "支付宝";
                    }
                },
                {
                    "data": "orderNote"
                },
                {
                    "data": "createTime"
                },
                {
                    "data": "orderStatus",
                    render:function(data,type,full){
                        var text = "<span class=\"label label-info\">待审核</span>";
                        switch(full.orderStatus){
                             case 2 :
                                text = "<span class='label label-warning'>处理中</span>";
                             break;
                             case 3:
                                text="<span class='label label-success'>已完成</span>";
                             break;
                             case 4:
                                text="<span class='label label-danger'>处理失败</span>";
                             break;
                             case 5 :
                                text="<span class='label label-default'>审核失败</span>";
                             break;
                        }
                        return text;
                    }
                }
                ,{

                    "data":"userId",
                    "render":function(data,type,full){
                        var html = "<a  href=\"javascript:;\" class=\"ajax-model-demo\" data-url=\"/main/memberview/userbehaviorrecord?user="+full.userId+"\" data-toggle=\"modal\"> 行为记录 </a>";
                        html +="<a  href=\"javascript:;\"> 金额记录 </a>";
                        if(full.orderStatus ==1){
                            html +="<a  href=\"javascript:;\" data-container=\"body\" data-toggle=\"confirmation-audit\" data-placement=\"top\" data-original-title=\"该订单是否通过审核\" data-popout=\"true\"> 审核  </a>"
                        }else if(full.orderStatus==5){
                            html +="<a  href=\"javascript:;\" data-container=\"body\" data-toggle=\"confirmation-audit\" data-placement=\"top\" data-original-title=\"该订单是否通过审核\" data-popout=\"true\"> 重审 </a>"
                        }

                       return html;
                    }
                }
            ],
            "fnDrawCallback": function (oSettings) {
                tx_order_audit();
                UpdateUIExtendedModals.init();
            }
        });
}
function tx_order_audit(){
    $("[data-toggle='confirmation-audit']").confirmation({
        singleton: true,
        popout: true,
        btnOkLabel: '通过',
        btnCancelLabel: '不通过',
        onConfirm: function () {
            //alert('1');
            App.blockUI();
            window.setTimeout(function () {
                App.unblockUI();
                bootbox.alert("Hello world!");
            }, 2000);
        } ,
        onCancel: function () {
            alert('2');
        }
    });
}
function table_refresh() {
            _table.api().draw(false);
        }
function table_load() {
            _table.api().draw(true);
        }
function right_menu(){
    //$('.dropdown-menu').off('click', 'ul li');
    //右键菜单
     $('tbody').on('mousedown', 'tr', function (e) {
          if (e.which == 3) {
             //var doclength = (document.documentElement.scrollTop) - 100;
            var linecode = $(this).find("input[type=checkbox]:first").val();
            var user_status=$(this).find(".user-status").data("status");
            if(user_status == 3){
                 $("#user-right-menu .dropdown-menu .forbid .forbid-text").text("解冻");
                 $("#user-right-menu .dropdown-menu .forbid a").attr("data-url","/main/memberview/userblocked?user="+linecode+"&status=1");
            }else{
                $("#user-right-menu .dropdown-menu .forbid .forbid-text").text("冻结");
                 $("#user-right-menu .dropdown-menu .forbid a").attr("data-url","/main/memberview/userblocked?user="+linecode+"&status=-1");
            }
            $("#user-right-menu .dropdown-menu .user-behavior a").attr("data-url","/main/memberview/userbehaviorrecord?user="+linecode);
            $("#user-right-menu .dropdown-menu .user-score a").attr("data-url","/main/memberview/userscore?user="+linecode);
            $("#user-right-menu .dropdown-menu .user-fans a").attr("data-url","/main/memberview/userfans?user="+linecode);
            $("#user-right-menu .dropdown-menu .user-note a").attr("data-url","/main/usermessage/usermsg?user="+linecode);
            $("#user-right-menu .dropdown-menu .user-reissue a").attr("data-url","/main/memberview/edituserscore?user="+linecode);
            $("#user-right-menu .dropdown-menu .change-deviced a").attr("data-url","/main/memberview/changedeviced?user="+linecode);
            $("#user-right-menu .dropdown-menu").css({ 'top': (e.pageY) + 'px', 'left': e.pageX - document.documentElement.scrollLeft + 'px' }).show();
                   
             $(":checkbox").removeAttr("checked");
             $(this).find("input[type=checkbox]").prop("checked", true);
            } else {
                $("#user-right-menu .dropdown-menu").hide();
        }
     });
}